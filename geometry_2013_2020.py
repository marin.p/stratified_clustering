from __init__ import stratification, preprocess_data, hc_optimal, get_medoids, get_medoids_dpe, kmeans, kmeans_optimal
from constants import periode_construction, region_dict, climate_zone_dict, mat_dict
import pandas as pd
import matplotlib.pyplot as plt
import time
start_time = time.time()


result_folder = r'C:\Users\pellan\OneDrive - CSTBGroup\Thèse stratégie carbone\Projets\Dynamic_modelling\Geometry_new_construction\results_clustering'

"""
STEP 1 : LOAD AND PREPARE THE DATA 

"""
### Load a df_population from BDNB. Here example for the 38 department with 46 columns
df_population = pd.read_pickle(r'C:\Users\pellan\OneDrive - CSTBGroup\Thèse stratégie carbone\Projets\Dynamic_modelling\Geometry_new_construction\residential_2013_2020_geometry.pkl')
print(df_population)
print('Loading the BDNB dataframe')
print("time elapsed: {:.2f}s".format(time.time() - start_time))

### We apply the dictionnaries
df_population['region'] = df_population['code_departement_insee'].apply(lambda x: region_dict.get(x, x))
df_population['climate_zone'] = df_population['code_departement_insee'].apply(lambda x: climate_zone_dict.get(x, x))
df_population['mat_mur_simpl'] = df_population['ffo_bat_mat_mur_txt'].apply(lambda x: mat_dict.get(x, x))

### Define our variables. The list can be different depending on the df !!
batiment_id = ['batiment_groupe_id']
surface = ['shab']
numerical_vars = ['s_geom_groupe', 'bdtopo_bat_hauteur_mean']
binary_vars = []
categorical_vars = ['ffo_bat_usage_niveau_1_txt', 'climate_zone', 'mat_mur_simpl']

all_vars = batiment_id  + surface + numerical_vars + binary_vars + categorical_vars


### We clean the NaN
df_population_filtered = df_population[all_vars]
df = df_population_filtered.dropna(subset=all_vars)


"""
STEP 2 : STRATIFICATION

"""

cat_cols=categorical_vars


stratified_population = stratification(df, cat_cols, surface, suffixes=('', '_strata'))
print('Stratify the df_population with the categorical variables')
print("time elapsed: {:.2f}s".format(time.time() - start_time))
print(stratified_population)

stratum_surface = stratified_population.groupby('stratum')['shab'].sum()
plt.figure()
plt.bar(stratum_surface.index, stratum_surface.values)
plt.title('Surface of each strata')
plt.xlabel('Stratum')
plt.ylabel('Surface in m2')
plt.show()


"""
STEP 3 : PREPROCESSING AND SPLIT THE STRATA IN DFs

"""

processeded_stratified_population = preprocess_data(
    stratified_population,

    numerical_cols=numerical_vars)

print('Preprocess the data and create a df for each strata')
print("time elapsed: {:.2f}s".format(time.time() - start_time))


# For loop to split the processeded_stratified_population into different df based on the stratum label
# We can call the different stratum_df with stratum_dfs[x]
locals_dict = {}

for value in processeded_stratified_population['stratum'].unique():
    new_df = processeded_stratified_population[processeded_stratified_population['stratum'] == value]
    df_name = 'stratum_df_' + str(value)
    locals_dict[df_name] = new_df

# Create a list of the created dataframes
stratum_dfs = [locals_dict[df_name] for df_name in locals_dict]


"""
STEP 4 : PERFORM KMEANS ON EVERY STRATA DFs

"""
# If we don't want to remove the strata with less than 2 elements, just remove the try-except
df_clustered_list = []
for i, df in enumerate(stratum_dfs):
    df_kmeans = kmeans_optimal(df, numerical_vars, num_clusters=None)
    df_clustered_list.append(df_kmeans)
df_clustered_concat = pd.concat(df_clustered_list, ignore_index=True)

print('Perform KMeans on each strata')
print("time elapsed: {:.2f}s".format(time.time() - start_time))



"""
STEP 5 : FIND THE MEDOID OF EACH CLUSTER IN EACH STRATA

"""

df_medoid = get_medoids(df_clustered_concat, "stratum", numerical_vars, surface='shab')

print('The end')
print("time elapsed: {:.2f}s".format(time.time() - start_time))


"""
STEP 6 : EXTRACT THE DF_MEDOID AND LIST OF BDNB_ID

"""
df_medoid = df_medoid.to_csv(result_folder + r'\test_geometry_kmeans_06072023.csv', sep=';')
#list_bdnb_id =
#dict_weight = dict(zip(df_medoid.batiment_groupe_id,df_medoid.pct_population))