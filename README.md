# Stratified hierarchical clustering 


## General information

From an extraction of the BDNB, the goal is first to stratify the population with categorical variables
and get the representativeness of the different stratas as % of the surface in the population. 

Then, in each stratas, we perform hierarchical clustering with numerical variables.

Lastly, to get a sample of the initial population, we take the medoid of each cluster in each stratas.

## Dependencies:
    -scikit-learn
    -scipy
    -fast cluster 
    -kneed


## Module 
    -constants.py : to get the region, climate zone, simplified wall materials and construction periods (FF)
    -Functions.py : list of functions used for stratification, prepropressing, 
    clustering and visualisation
    -Main.py : example of implementation for 38 department



## Description of the functions 
1) **def stratification**(df, cat_cols, surface_col): 
Enables to stratify the population with categorical variables 
Returns the df with 2 additional columns : a strata label (from 1 to n), and name corresponding to 


2) **def preprocess_data**(df, binary_cols, numerical_cols)
Preprocess the binary and numerical columns using respectively LabelEncoding and StandardScaler


3) **def hc**(df, num_cols, linkage_method='ward', distance_metric='euclidean', max_clusters=50, plot_elbow=True):
Perform hierarchical clustering on numerical variables with the max_clusters number given
Return a dataframe with an additional cluster label and a linkage_matrix 


4) **def hc_optimal**(df, num_cols, linkage_method='ward', distance_metric='euclidean', max_clusters=50, plot_elbow=True,
                 df_name=None)
Perform hierarchical clustering on numerical variables with the number of clusters given by the Elbow method
Skip the df with less than 3 elements
Return a dataframe with an additional cluster label and a linkage_matrix


5) **def hc_optimal_2**(df, num_cols, linkage_method='ward', distance_metric='euclidean', max_clusters=50, plot_elbow=True,
                 df_name=None)
Perform hierarchical clustering on numerical variables with the number of clusters given by the Elbow method
If the df has less than 3 elements, we assign automatically 1 cluster only
Return a dataframe with an additional cluster label and a linkage_matrix


6) **def plot_dendogram**(linkage_matrix)
Plot a dendrogram with a given linkage_matrix


7) **def get_medoids**(df, strata_col, numerical_cols, surface)
Get the medoids of each cluster and return its surface weight in the cluster, the strata and the all population


8) **def get_medoids_dpe**(df, strata_col, cluster_col, numerical_cols, nan_col, surface)
Get the medoids of each cluster and return its surface weight in the cluster, the strata and the all population
It should choose the buildings with real DPE