from __init__ import stratification, preprocess_data, hc_optimal, get_medoids, get_medoids_dpe
from constants import periode_construction, region_dict, climate_zone_dict, mat_dict
import pandas as pd
import matplotlib.pyplot as plt
import time
start_time = time.time()

"""
STEP 1 : LOAD AND PREPARE THE DATA 

"""
### Load a df_population from BDNB. Here example for the 38 department with 46 columns
df_population = pd.read_pickle(r'bdnb_v07_38_46_cols.pkl')
print(df_population)
print('Loading the BDNB dataframe')
print("time elapsed: {:.2f}s".format(time.time() - start_time))

### We apply the dictionnaries
df_population['periode_construction'] = df_population.ffo_bat_annee_construction.apply(lambda x:(periode_construction(x)))
df_population['region'] = df_population['code_departement_insee'].apply(lambda x: region_dict.get(x, x))
df_population['climate_zone'] = df_population['code_departement_insee'].apply(lambda x: climate_zone_dict.get(x, x))
df_population['mat_mur_simpl'] = df_population['ffo_bat_mat_mur_txt'].apply(lambda x: mat_dict.get(x, x))

### Filter on residential buildings (optional)
df_population_res = df_population[df_population['ffo_bat_usage_principal'].isin(["maison individuelle", "batiment collectif d'habitation"])]
df_population_res = df_population_res[df_population_res['ffo_bat_usage_niveau_1_txt'].isin(["Résidentiel individuel", "Résidentiel collectif"])]

### Filter on number

### Define our variables. The list can be different depending on the df !!
batiment_id = ['batiment_groupe_id']
surface = ['ffo_bat_s_log']
numerical_vars = ['ffo_bat_nb_log', 'ffo_bat_s_loc_tot', 'ffo_bat_nb_log_vac',
                  'bdtopo_bat_hauteur_mean', 'bdtopo_bat_altitude_sol_mean',
                  'simu_dpe_etat_initial_consommation_energie_estim_mean',
                  'simu_dpe_etat_renove_consommation_energie_estim_mean',
                  'simu_dpe_gisement_gain_ges_mean']
binary_vars = ['ffo_bat_usage_principal']
categorical_vars = ['periode_construction', 'simu_dpe_etiquette_dpe_initial_map', 'mat_mur_simpl']
dpe_var = ['dpe_logtype_numero_dpe']

### Define which variables can be NaN
all_vars = batiment_id  + surface + numerical_vars + binary_vars + categorical_vars + dpe_var
non_nan_vars = batiment_id + surface + numerical_vars + binary_vars + categorical_vars
ok_nan_vars = dpe_var

### We merge the variables
df_population_both = df_population_res[all_vars]
df = df_population_both.dropna(subset=non_nan_vars)


"""
STEP 2 : STRATIFICATION

"""

cat_cols=categorical_vars+binary_vars


stratified_population = stratification(df, cat_cols, surface)
print('Stratify the df_population with the categorical variables')
print("time elapsed: {:.2f}s".format(time.time() - start_time))


stratum_surface = stratified_population.groupby('stratum')['ffo_bat_s_log_x'].sum()
plt.figure()
plt.bar(stratum_surface.index, stratum_surface.values)
plt.title('Surface of each strata')
plt.xlabel('Stratum')
plt.ylabel('Surface in m2')
plt.show()


"""
STEP 3 : PREPROCESSING AND SPLIT THE STRATA IN DFs

"""

processeded_stratified_population = preprocess_data(
    stratified_population, binary_cols=binary_vars, numerical_cols=numerical_vars)

print('Preprocess the data and create a df for each strata')
print("time elapsed: {:.2f}s".format(time.time() - start_time))


# For loop to split the processeded_stratified_population into different df based on the stratum label
# We can call the different stratum_df with stratum_dfs[x]
locals_dict = {}

for value in processeded_stratified_population['stratum'].unique():
    new_df = processeded_stratified_population[processeded_stratified_population['stratum'] == value]
    df_name = 'stratum_df_' + str(value)
    locals_dict[df_name] = new_df

# Create a list of the created dataframes
stratum_dfs = [locals_dict[df_name] for df_name in locals_dict]

"""
STEP 4 : PERFORM HIERARCHICAL CLUSTERING ON EVERY STRATA DFs

"""
# If we don't want to remove the strata with less than 2 elements, just remove the try-except
df_clustered_list = []
for i, df in enumerate(stratum_dfs):
    try:
        linkage_matrix, df_clustered = hc_optimal(df, num_cols=numerical_vars, df_name=f'df{i}', plot_elbow=False)
        df_clustered_list.append(df_clustered)
        # plot dendrogram
        #plt.figure(figsize=(10, 5))
        #dend = sch.dendrogram(linkage_matrix, truncate_mode='lastp', distance_sort='ascending')
        #plt.title(f"Dendrogram for dataframe {i}")
        #plt.xlabel("Samples")
        #plt.ylabel("Distances")
        #plt.show()
    except TypeError:
        print(f"Skipping dataframe with shape {df.shape}")
        continue
df_clustered_concat = pd.concat(df_clustered_list, ignore_index=True)

print('Perform hierarchical clustering on each strata')
print("time elapsed: {:.2f}s".format(time.time() - start_time))


"""
STEP 5 : FIND THE MEDOID OF EACH CLUSTER IN EACH STRATA

"""

df_medoid = get_medoids(df_clustered_concat, "stratum", numerical_vars, surface='ffo_bat_s_log_x')
df_medoid_dpe = get_medoids_dpe(df_clustered_concat, strata_col='stratum', cluster_col='cluster',
                                numerical_cols=numerical_vars, nan_col=dpe_var, surface='ffo_bat_s_log_x')

print('The end')
print("time elapsed: {:.2f}s".format(time.time() - start_time))


"""
STEP 6 : EXTRACT THE DF_MEDOID AND LIST OF BDNB_ID

"""
#df_medoid = df_medoid.to_pickle(r'')
#list_bdnb_id =
#dict_weight = dict(zip(df_medoid.batiment_groupe_id,df_medoid.pct_population))