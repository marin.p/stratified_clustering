region_dict = {
    'Auvergne-Rhône-Alpes': ['01', '03', '07', '15', '26', '38', '42', '43', '63', '69', '73', '74'],
    'Bourgogne-Franche-Comté': ['21', '25', '39', '58', '70', '71', '89', '90'],
    'Bretagne': ['35', '22', '56', '29'],
    'Centre-Val de Loire': ['18', '28', '36', '37', '41', '45'],
    'Corse': ['2A', '2B'],
    'Grand Est': ['08', '10', '51', '52', '54', '55', '57', '67', '68', '88'],
    'Guadeloupe': ['971'],
    'Guyane': ['973'],
    'Hauts-de-France': ['02', '59', '60', '62', '80'],
    'Île-de-France': ['75', '77', '78', '91', '92', '93', '94', '95'],
    'La Réunion': ['974'],
    'Martinique': ['972'],
    'Normandie': ['14', '27', '50', '61', '76'],
    'Nouvelle-Aquitaine': ['16', '17', '19', '23', '24', '33', '40', '47', '64', '79', '86', '87'],
    'Occitanie': ['09', '11', '12', '30', '31', '32', '34', '46', '48', '65', '66', '81', '82'],
    'Pays de la Loire': ['44', '49', '53', '72', '85'],
    'Provence-Alpes-Côte d\'Azur': ['04', '05', '06', '13', '83', '84'],
    'Inconnu' : [1]
}

climate_zone_dict = {
        '01': "H1c",
        '02': "H1a",
        '03': "H1c",
        '04': "H2d",
        '05': "H1c",
        '06': "H3",
        '07': "H2d",
        '08': "H1b",
        '09': "H2c",
        '10': "H1b",
        '11': "H3",
        '12': "H2c",
        '13': "H3",
        '14': "H1a",
        '15': "H1c",
        '16': "H2b",
        '17': "H2b",
        '18': "H2b",
        '19': "H1c",
        '20': "H3",
        '2A': "H3",
        '2B': "H3",
        '21': "H1c",
        '22': "H2a",
        '23': "H1c",
        '24': "H2c",
        '25': "H1c",
        '26': "H2d",
        '27': "H1a",
        '28': "H1a",
        '29': "H2a",
        '30': "H3",
        '31': "H2c",
        '32': "H2c",
        '33': "H2c",
        '34': "H3",
        '35': "H2a",
        '36': "H2b",
        '37': "H2b",
        '38': "H1c",
        '39': "H1c",
        '40': "H2c",
        '41': "H2b",
        '42': "H1c",
        '43': "H1c",
        '44': "H2b",
        '45': "H1b",
        '46': "H2c",
        '47': "H2c",
        '48': "H2d",
        '49': "H2b",
        '50': "H2a",
        '51': "H1b",
        '52': "H1b",
        '53': "H2b",
        '54': "H1b",
        '55': "H1b",
        '56': "H2a",
        '57': "H1b",
        '58': "H1b",
        '59': "H1a",
        '60': "H1a",
        '61': "H1a",
        '62': "H1a",
        '63': "H1c",
        '64': "H2c",
        '65': "H2c",
        '66': "H3",
        '67': "H1b",
        '68': "H1b",
        '69': "H1c",
        '70': "H1b",
        '71': "H1c",
        '72': "H2b",
        '73': "H1c",
        '74': "H1c",
        '75': "H1a",
        '76': "H1a",
        '77': "H1a",
        '78': "H1a",
        '79': "H2b",
        '80': "H1a",
        '81': "H2c",
        '82': "H2c",
        '83': "H3",
        '84': "H2d",
        '85': "H2b",
        '86': "H2b",
        '87': "H1c",
        '88': "H1b",
        '89': "H1b",
        '90': "H1b",
        '91': "H1a",
        '92': "H1a",
        '93': "H1a",
        '94': "H1a",
        '95': "H1a",
        '971': "non applicable",
        '972': "non applicable",
        '973': "non applicable",
        '974': "non applicable",
        '975': "non applicable",
        '976': "non applicable",
    }

mat_dict = {
        'PIERRE': 'pierre',
        'PIERRE - AUTRES': 'pierre',

        'MEULIERE': 'meuliere',
        'MEULIERE - PIERRE': 'meuliere',
        'MEULIERE - AUTRES': 'meuliere',

        'BETON - PIERRE': 'beton',
        'BETON - MEULIERE': 'beton',
        'BETON': 'beton',
        'BETON - AUTRES': 'beton',
        'BETON - BOIS': 'beton',
        'BETON - BRIQUES': 'beton',

        'AGGLOMERE': 'agglo',
        'AGGLOMERE - BOIS': 'agglo',
        'AGGLOMERE - BETON': 'agglo',
        'AGGLOMERE - MEULIERE': 'agglo',
        'AGGLOMERE - AUTRES': 'agglo',
        'AGGLOMERE - BRIQUES': 'agglo',
        'AGGLOMERE - PIERRE': 'agglo',

        'BOIS': 'bois',
        'BOIS - PIERRE': 'bois',
        'BOIS - MEULIERE': 'bois',
        'BOIS - AUTRES': 'bois',
        'BOIS - BRIQUES': 'bois',

        'BRIQUES': 'briques',
        'BRIQUES - MEULIERE': 'briques',
        'BRIQUES - PIERRE': 'briques',
        'BRIQUES - AUTRES': 'briques',

        'INDETERMINE': 'autres',
        'AUTRES': 'autres',

}

def periode_construction(x):
    if x<=1918:
        res='P0'
    elif 1919<=x<=1945:
        res='P1'
    elif 1946<=x<=1970:
        res='P2'
    elif 1971<=x<=1990:
        res='P3'
    elif 1991<=x<=2005:
        res='P4'
    elif 2006<=x<=2012:
        res='P5'
    elif x>2012:
        res='P6'
    elif x>2022:
        res='P7'
    else:
        res='NaN'
    return res

